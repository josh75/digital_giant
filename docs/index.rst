.. Digital Giant documentation master file, created by
   sphinx-quickstart on Thu Jan  3 18:52:34 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hex's Digital Giant documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about  

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
