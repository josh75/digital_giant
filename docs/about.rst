Digital Giant is a multi-discipline imaging and production studio cofounded by friends and experienced industry professionals Dallas Carroll and Alex Pickup, working primarily out of Los Angeles and London.

The Digital Giant ethos is to approach every project with freshness and adaptability, using our broad knowledge of production methods and processes to achieve great results and make things as straight forward and streamlined as is possible.

In simple terms, we just love working with our fellow artists to create imagery of all kinds, we relish the process as much as the outcome, and we believe that the whole event should be fun and rewarding for everybody involved.
